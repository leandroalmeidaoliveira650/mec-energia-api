import pytest
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType

def run_user_type_test(user_type, expected_error_message, user_model):
    with pytest.raises(Exception) as context:
        UserType.is_valid_user_type(user_type, user_model=user_model)

    actual_error_message = str(context.value)
    assert actual_error_message == expected_error_message, f"Esperado: {expected_error_message}, Obtido: {actual_error_message}"

def test_user_type_validation():
    test_cases = [
        ('not_defined', 'User type (not_defined) does not exist', UniversityUser),
        ('super_user', 'Wrong User type (super_user) for this Model User (<class \'users.models.UniversityUser\'>)', UniversityUser),
        ('university_user', 'Wrong User type (university_user) for this Model User (<class \'users.models.CustomUser\'>)', CustomUser),
    ]

    for user_type, expected_error_message, user_model in test_cases:
        run_user_type_test(user_type, expected_error_message, user_model)



