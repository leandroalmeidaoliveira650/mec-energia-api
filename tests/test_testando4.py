import pytest
from utils.cnpj_validator_util import CnpjValidator

def test_remover_formatacao_valid_cnpj():
    cnpj_with_format = "12.345.678/0001-09"
    result = CnpjValidator.remover_mascara(cnpj_with_format)
    assert result == "12345678000109"
        
def test_remover_formatacao_same_digits_cnpj():
    cnpj_with_format = "22.222.222/2222-22"
    with pytest.raises(ValueError):
        CnpjValidator.remover_mascara(cnpj_with_format)

def test_remover_formatacao_short_cnpj():
    cnpj_with_format = "12.345.678/0001-0"
    with pytest.raises(ValueError):
        CnpjValidator.remover_mascara(cnpj_with_format)

def test_remover_formatacao_long_cnpj():
    cnpj_with_format = "12.345.678/0001-0987654321"
    with pytest.raises(ValueError):
        CnpjValidator.remover_mascara(cnpj_with_format)